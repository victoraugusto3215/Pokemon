//
//  ListaAlunosViewController.swift
//
//  Created by COTEMIG 04/08/22.
//

import UIKit

class ListaViewController: UIViewController, UITableViewDataSource {
    
    struct pokemom {
        var nome: String
        var habilidade1: String
        var habilidade2: String
    }
    
    @IBOutlet weak var tableView: UITableView!
    private var listaDePokemom = [pokemom]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.iniciarAlunos()
        self.tableView.dataSource = self
    }
    
    func iniciarAlunos() {
        self.listaDePokemom = [
            pokemom(nome: "Bulbassauro", habilidade1: <#T##String#>: "Gramia"),
            pokemom(nome: "Bulbassauro", habilidade1: <#T##String#>: "Gramia"),
            pokemom(nome: "Bulbassauro", habilidade1: <#T##String#>: "Gramia"),
            pokemom(nome: "Bulbassauro", habilidade1: <#T##String#>: "Gramia"),
            pokemom(nome: "Bulbassauro", habilidade1: <#T##String#>: "Gramia"),
            pokemom(nome: "Bulbassauro", habilidade1: <#T##String#>: "Gramia"),
            pokemom(nome: "Bulbassauro", habilidade1: <#T##String#>: "Gramia"),
            pokemom(nome: "Bulbassauro", habilidade1: <#T##String#>: "Gramia"),
            pokemom(nome: "Bulbassauro", habilidade1: <#T##String#>: "Gramia"),
        ]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Aluno", for: indexPath) as? AlunoTableViewCell {
            cell.alunoLabel.text = self.listaDeAlunos[indexPath.row].nome
            cell.matriculaLabel.text = self.listaDeAlunos[indexPath.row].nome
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDePokemom.count
    }
    
    func sairDaTela() {
        self.dismiss(animated: true)
    }
}

